import { Button } from '../Button/Button';
import { Card } from '../Card/Card';
import s from './ErrorModal.module.css';
import ReactDOM from 'react-dom';

const Backdrop = ({ dismissModal }) => {
    return <div className={s.backdrop} onClick={dismissModal} />
}

const ModalOverlay = ({ dismissModal, title, message }) => {
    return (
        <Card className={s.modal}>
            <header className={s.header}>
                <h2>{title}</h2>
            </header>
            <div className={s.content}>
                <p className="">{message}</p>
            </div>
            <footer className={s.actions}>
                <Button dismissModal={dismissModal}>Okay</Button>
            </footer>
        </Card>
    );
}

export const ErrorModal = ({ title, message, dismissModal }) => {
    return (
        <>
            {ReactDOM.createPortal(
                <Backdrop dismissModal={dismissModal} />,
                document.getElementById('backdrop-root')
            )}
            {ReactDOM.createPortal(
                <ModalOverlay title={title} message={message} dismissModal={dismissModal} />,
                document.getElementById('overlay-root')
            )}
        </>
    )
}