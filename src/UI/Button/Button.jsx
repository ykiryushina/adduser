import s from './Button.module.css';

export const Button = ({ type, dismissModal, children }) => {
    return (
        <button
            className={s.button}
            type={type || 'button'}
            onClick={dismissModal}
        >
            {children}
        </button>
    )
}