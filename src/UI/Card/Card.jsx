import s from './Card.module.css';

export const Card = ({ children, className }) => {
    return (
        <div className={`${s.card} ${className}`}>{children}</div>
    )
}