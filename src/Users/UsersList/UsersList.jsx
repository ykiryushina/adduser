import { Card } from '../../UI/Card/Card';
import s from './UsersList.module.css';

export const UsersList = ({ users }) => {
    return (
        <Card className={s.users}>
            <ul>
                {users.map((user, index) => (
                    <li key={user + index + user.age}>{user.name} ({user.age} years old)</li>
                ))}
            </ul>
        </Card>

    )
}