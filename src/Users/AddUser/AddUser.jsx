import { useRef, useState } from "react";
import { Wrapper } from "../../Helpers/Wrapper/Wrapper";
import { Button } from "../../UI/Button/Button";
import { Card } from "../../UI/Card/Card";
import { ErrorModal } from "../../UI/ErrorModal/ErrorModal";
import s from './AddUser.module.css';
export const AddUser = ({ onAddUser }) => {
    const [error, setError] = useState();
    const nameInputRef = useRef();
    const ageInputRef = useRef();

    const addUserHandler = (event) => {
        event.preventDefault();
        const enteredUserName = nameInputRef.current.value;
        const enteredAge = ageInputRef.current.value;
        if (enteredUserName.trim().length === 0 || enteredAge.trim().length === 0) {
            setError({
                title: 'Invalid input',
                message: 'Please enter a valid name and age (non-empty values).'
            });
            return;
        }
        if (+enteredAge < 1) {
            setError({
                title: 'Invalid age',
                message: 'Please enter a valid age (> 0).'
            });
            return;
        }
        onAddUser(enteredUserName, enteredAge);
        nameInputRef.current.value = '';
        ageInputRef.current.value = '';
    }

    const dismissModal = () => {
        setError(null);
    }

    return (
        <Wrapper>
            {error && <ErrorModal title={error.title} message={error.message} dismissModal={dismissModal} />}
            <Card className={s.input}>
                <form onSubmit={addUserHandler}>
                    <label htmlFor="username">Username</label>
                    <input
                        id="username"
                        type="text"
                        ref={nameInputRef} />
                    <label htmlFor="age">Age (years)</label>
                    <input
                        id="age"
                        type="number"
                        ref={ageInputRef} />
                    <Button type="submit">Add User</Button>
                </form>
            </Card>
        </Wrapper>

    );
}