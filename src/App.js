import React from 'react';
import { useState } from 'react';
import { AddUser } from './Users/AddUser/AddUser';
import { UsersList } from './Users/UsersList/UsersList';


function App() {
  const [usersList, setUsersList] = useState([]);

  const addUserHandler = (username, age) => {
    setUsersList(prevUsersList => {
      return [...prevUsersList, { name: username, age: age }]
    });
  }

  return (
    <>
      <AddUser onAddUser={addUserHandler} />
      <UsersList users={usersList} />
    </>
  );
}

export default App;
